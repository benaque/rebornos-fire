# RebornOS FIRE (Features and Improvements in RebornOS made Easy)

## Build and Sign the Package (for Developers)

```
makepkg -g >> PKGBUILD && makepkg
gpg --detach-sign --use-agent {file_name}
```

## Purpose
To make tasks that are commonly more advanced (need interaction with a terminal for) possible for a newbie who may not be comfortable in an Arch-based world at first.

### Functions

- Easily manage the most common display managers (LightDM and SDDM) which lack simple GUI management applications.
![](Screenshots/DisplayManagers.png)

- Install additional DEs if so desired.
![](Screenshots/DesktopEnvironments.png)

- Clean out your system using `pacman`-specific commands. For instance, cleaning the package cache and removing unneccesary (often orphaned) packages.
![](Screenshots/SystemMaintenance.png)
![](Screenshots/Repair.png)

- Rollback your entire system to a previous date.
![](Screenshots/Rollback.png)

- Expose the user to a few simple applications currently found in the AUR that can be used to make tasks easier in Arch Linux.
![](Screenshots/Addons.png)
![](Screenshots/Tools.png)

### Install

#### RebornOS
1) Open up your favorite Software Installer (most likely Pamac) and search for "reborn-updates", clicking `Install`. If you prefer a terminal, just enter the below text:
```
sudo pacman -S rebornos-fire --noconfirm
```
DONE!

#### Arch Linux (or any derivative)
1) Navigate to our Gitlab page here (which you are obviously already at since you are reading this) by the following URL: https://gitlab.com/reborn-os-team/reborn-updates-and-maintenance

2) Download the `rebornos-fire` package (the only file ending in .zst).

3) Install the downloaded file either by right-clicking on it and selecting to "Open With..." your favorite Software Installer, or just through the terminal using the below command:
```
sudo pacman -U ${PATH_TO_FILE}
```
4) A new version came out since you last did this? No problem! If you are using a non-RebornOS derivitive of Arch Linux (or Arch itself), RebornOS FIRE will automatically determine whether or not an update is available, and update for you when you next launch it if necessary.
