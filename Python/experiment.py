# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
import time
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

gladeFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Glade', 'experiment.glade'))

# Create Handlers (Triggers) for each item
class Handler:
    def __init__(self):

    # Close the window
        def onDestroy(self, *args):
            Gtk.main_quit()

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(gladeFile)
builder.connect_signals(Handler())

window2 = builder.get_object("Dialog")
window2.show_all()

Gtk.main()
